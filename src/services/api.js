/* eslint-disable no-new */
/* eslint-disable class-methods-use-this */
import decode from 'jwt-decode';
import { Urls } from '../constants';

export default class ApiService {
  // Initializing important variables
  constructor() {
    this.domain = Urls.BASE_URL;
    this.account = Urls.ACCOUNT_URL;
    this.FILE_UPLOAD_URL = Urls.FILE_UPLOAD_URL;
    this.VENDORS_URL = Urls.VENDORS_URL;
    this.USER_URL = Urls.USER_URL;
    this.fetch = this.fetch.bind(this);
  }

  getEmployee = filters => {
    return this.fetch(`${this.domain}${Urls.URLS.employees}?${filters}`).then(res => {
      return Promise.resolve(res);
    });
  };

  getVendor = filters => {
    return this.fetch(`${this.domain}${Urls.URLS.vendors}?${filters}`).then(res => {
      return Promise.resolve(res);
    });
  };

  createVendor = filter => {
    return this.fetch(`${this.domain}${Urls.URLS.createVendor}`, {
      method: 'POST',
      body: JSON.stringify(filter),
    }).then(res => {
      return Promise.resolve(res);
    });
  };

  uploadImg = filter => {
    this.isFileUpload = true;
    return this.fetch(`${this.FILE_UPLOAD_URL}${Urls.URLS.imgUpload}`, {
      method: 'POST',
      body: filter,
    }).then(res => {
      this.isFileUpload = false;

      return Promise.resolve(res);
    });
  };

  getAPIRequest = filter => {
    return this.fetch(`${this.domain}${filter.apiURL}`).then(res => {
      return Promise.resolve(res);
    });
  };

  postAPIRequest = filter => {
    return this.fetch(`${this.domain}${filter.apiURL}`, {
      method: 'POST',
      body: JSON.stringify(filter.request),
    }).then(res => Promise.resolve(res));
  };

  postAPIRequest = filter => {
    return this.fetch(`${this.domain}${filter.apiURL}`, {
      method: 'POST',
      body: JSON.stringify(filter.request),
    }).then(res => Promise.resolve(res));
  };

  postVendorAPIRequest = filter => {
    return this.fetch(`${this.VENDORS_URL}${filter.apiURL}`, {
      method: 'POST',
      body: JSON.stringify(filter.request),
    }).then(res => Promise.resolve(res));
  };

  getOffer = filters => {
    return this.fetch(`${this.domain}${Urls.URLS.offers}?${filters}`).then(res => {
      return Promise.resolve(res);
    });
  };
  getVendorDetail = vendorId => {
    return this.fetch(`${this.domain}${Urls.URLS.vendors}/${vendorId}`).then(res => {
      return Promise.resolve(res);
    });
  };

  getCategories = search => {
    return this.fetch(`${this.USER_URL}${Urls.URLS.categories}?search=${search}`).then(res => {
      return Promise.resolve(res);
    });
  };
  toggleEmployeActive = (userId, isActive) => {
    return this.fetch(`${this.domain}${Urls.URLS.employees}`, {
      method: 'PUT',
      body: JSON.stringify({
        user_id: userId,
        is_active: isActive,
      }),
    }).then(res => {
      return Promise.resolve(res);
    });
  };

  updateOffer = update => {
    return this.fetch(`${this.domain}${Urls.URLS.offers}`, {
      method: 'PUT',
      body: JSON.stringify(update),
    }).then(res => {
      return Promise.resolve(res);
    });
  };

  myProfile = () => {
    return this.fetch(`${this.account}${Urls.URLS.profile}`, {
      method: 'POST',
    }).then(res => {
      return Promise.resolve(res);
    });
  };

  updateVendor = update => {
    return this.fetch(`${this.domain}${Urls.URLS.vendors}`, {
      method: 'PUT',
      body: JSON.stringify(update),
    }).then(res => {
      return Promise.resolve(res);
    });
  };

  newCredential = (name, mobile, role) => {
    // Get a token from api server using the fetch api
    return this.fetch(`${this.domain}${Urls.URLS.newCredentials}`, {
      method: 'POST',
      body: JSON.stringify({
        mobile,
        name,
        role,
      }),
    }).then(res => {
      return Promise.resolve(res);
    });
  };

  tokenVerify = token => {
    // Get a token from api server using the fetch api
    return this.fetch(`${this.domain}${Urls.URLS.tokenVerify}`, {
      method: 'POST',
      body: JSON.stringify({
        token,
      }),
    }).then(res => {
      return Promise.resolve(res);
    });
  };

  login = (mobile, password) => {
    console.log('jjjjjs');
    // Get a token from api server using the fetch api
    return this.fetch(`${this.account}${Urls.URLS.login}`, {
      method: 'POST',
      body: JSON.stringify({
        mobile,
        password,
      }),
    }).then(res => {
      if (res.code === 200) {
        this.setToken(res.data); // Setting the token in localStorage
      }
      return Promise.resolve(res);
    });
  };

  changePassword = (oldPassword, newPassword) => {
    return this.fetch(`${this.account}${Urls.URLS.changePassword}`, {
      method: 'PUT',
      body: JSON.stringify({
        old_password: oldPassword,
        new_password: newPassword,
      }),
    }).then(res => {
      return Promise.resolve(res);
    });
  };

  loggedIn = () => {
    // Checks if there is a saved token and it's still valid
    const token = this.getToken(); // GEtting token from localstorage
    return !!token && !this.isTokenExpired(token); // handwaiving here
  };

  isTokenExpired = token => {
    try {
      this.decoded = decode(token);
      console.log(this.decoded);
      if (this.decoded.exp < Date.now() / 1000) {
        // Checking if token is expired. N
        return true;
      }
      return false;
    } catch (err) {
      return false;
    }
  };

  setToken = data => {
    // Saves user token to localStorage
    localStorage.setItem('ut', data.token);
    localStorage.setItem('nm', data.name);
    localStorage.setItem('im', data.image);
    localStorage.setItem('rl', data.role);
  };

  getToken = () => {
    // Retrieves the user token from localStorage
    return localStorage.getItem('ut');
  };

  getRole() {
    // Retrieves the user role from localStorage
    return localStorage.getItem('rl');
  }

  logout = () => {
    // Clear user token and profile data from localStorage
    localStorage.removeItem('ut');
  };

  getProfile = () => {
    // Using jwt-decode npm package to decode the token
    return decode(this.getToken());
  };

  fetch = (url, options) => {
    // performs api calls sending the required authentication headers
    let headers = {};
    if (!this.isFileUpload) {
      headers = {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      };
    }

    // Setting Authorization header
    // Authorization: Bearer xxxxxxx.xxxxxxxx.xxxxxx
    if (this.loggedIn()) {
      headers.Authorization = `Bearer ${this.getToken()}`;
    }

    return (
      fetch(url, {
        headers,
        ...options,
      })
        // .then(this.checkStatus)
        .then(response => response.json())
    );
  };

  // eslint-disable-next-line consistent-return
  checkStatus = response => {
    // raises an error in case response status is not a success
    if (response.status >= 200 && response.status < 300) {
      // Success status lies between 200 to 300
      return response;
    }
    console.log(response);
    // eslint-disable-next-line prefer-const
    // let error = new Error(response.statusText);

    // return response;
    return Promise.reject(response);
    // error.response = response;
    // throw error;
  };
}
