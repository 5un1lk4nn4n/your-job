/* eslint-disable no-unused-vars */
import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import App from '../App';
import { withCookies, Cookies } from 'react-cookie';

const Protected = ({ component: Component, cookies, ...rest }) => {
  const token = cookies.get('emid');
  if (token) {
    return <Route render={() => <Component {...rest} />} />;
  }
  return <Redirect to={{ pathname: '/login', state: { from: rest.location.pathname } }} />;
};

export default withCookies(Protected);
