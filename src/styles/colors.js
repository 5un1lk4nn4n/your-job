export const primary = '#040403';
export const secondary = '#0a0908';
export const tertiary = '#383d3b';
export const quartenary = '#eee5e9';
export const background = '#edece6';
export const backgrounder = '#fafafc';
export const light = '#f7f0f5';

// 9b9baa
export const white = '#fff';
export const black = '#040403';
