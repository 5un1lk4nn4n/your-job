export const BASE_URL = 'https://api.epeoplemart.com/api/epmarts/';
export const ACCOUNT_URL = 'https://api.epeoplemart.com/api/accounts/';
export const FILE_UPLOAD_URL = 'https://api.epeoplemart.com/api/files/';
export const VENDORS_URL = 'https://api.epeoplemart.com/api/vendors/';
export const MEDIA_URL = 'https://api.epeoplemart.com/static/media/';
export const USER_URL = 'https://api.epeoplemart.com/api/users/';

export const URLS = {
  login: 'auth/login',
  newCredentials: 'create-new-credintial',
  employees: 'employees',
  vendors: 'vendors',
  offers: 'offers',
  createVendor: 'register-vendor',
  imgUpload: 'upload',
  profile: 'me',
  changePassword: 'change-password',
  categories: 'categories',
};
