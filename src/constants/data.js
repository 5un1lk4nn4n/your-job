export const company = [
  {
    id: 1,
    name: 'Tesla Inc',
    location: '456, Top Street, Delhi',
    website: 'www.tesla.com',
    employees: 300,
    category: 'automobile',
    branches: [
      { address: '24, South Street, Hyderabad', phone: '02449845' },
      { address: '24, South Street, Chennai', phone: '02449845' },
    ],
  },
  {
    id: 2,
    name: 'Microsoft Inc',
    location: '456, Top Street, Delhi',
    website: 'www.microsoft.com',
    employees: 400,
    category: 'Software',
    branches: [
      { address: '24, South Street, Hyderabad', phone: '02449845' },
      { address: '24, South Street, Chennai', phone: '02449845' },
    ],
  },
  {
    id: 3,
    name: 'Sony Inc',
    location: '456, Top Street, Delhi',
    website: 'www.tesla.com',
    employees: 8,
    category: 'Entertainment',
    branches: [
      { address: '24, South Street, Hyderabad', phone: '02449845' },
      { address: '24, South Street, Chennai', phone: '02449845' },
    ],
  },
  {
    id: 4,
    name: 'Solar City Inc',
    location: '456, Top Street, Delhi',
    website: 'www.solarcity.com',
    employees: 300,
    category: 'Energy',
    branches: [
      { address: '24, South Street, Hyderabad', phone: '02449845' },
      { address: '24, South Street, Chennai', phone: '02449845' },
    ],
  },
  {
    id: 5,
    name: 'Google Inc',
    location: '456, Top Street, Delhi',
    website: 'www.google.com',
    employees: 300,
    category: 'automobile',
    branches: [
      { address: '24, South Street, Hyderabad', phone: '02449845' },
      { address: '24, South Street, Chennai', phone: '02449845' },
    ],
  },
  {
    id: 6,
    name: 'Real Me Inc',
    location: '456, Top Street, Delhi',
    website: 'www.tesla.com',
    employees: 300,
    category: 'automobile',
    branches: [
      { address: '24, South Street, Hyderabad', phone: '02449845' },
      { address: '24, South Street, Chennai', phone: '02449845' },
    ],
  },
  {
    id: 7,
    name: 'Glass Inc',
    location: '456, Top Street, Delhi',
    website: 'www.tesla.com',
    employees: 300,
    category: 'automobile',
    branches: [
      { address: '24, South Street, Hyderabad', phone: '02449845' },
      { address: '24, South Street, Chennai', phone: '02449845' },
    ],
  },
];

export const employees = [
  {
    id: 1,
    name: 'Barry Allen',

    mobile: '9876543210',
    password: '123456',
    token: '1233445',
    experiance: [
      {
        company: 1,
        isCurrent: true,
        duration: {
          from: '21-06-2019',
          to: 'present',
        },
        position: 'Senior Software engineer',
      },
      {
        company: 2,
        isCurrent: false,
        duration: {
          from: '21-06-2018',
          to: '20-06-2019',
        },
        position: 'Software engineer',
      },
    ],
  },
  {
    id: 2,
    name: 'Will',

    mobile: '9898987123',
    password: '123456',
    token: '1233445',
    experiance: [
      {
        company: 2,
        isCurrent: true,
        duration: {
          from: '21-06-2019',
          to: 'present',
        },
        position: 'Senior Software engineer',
      },
      {
        company: 4,
        isCurrent: false,
        duration: {
          from: '21-06-2018',
          to: '20-06-2019',
        },
        position: 'Software engineer',
      },
    ],
  },
];
