import React, { Component } from 'react';
import { Box, Heading, Text } from 'grommet';
import { withCookies } from 'react-cookie';
import { withRouter } from 'react-router-dom';
import { CaretNext, Phone } from 'grommet-icons';
import { company, employees } from '../constants/data';
import TopBar from '../components/TopBar';

/***
 * Employee main Page
 */
class EmployeeDashboard extends Component {
  empId = null;
  isFound = false;
  constructor(props) {
    super(props);
    const { empId } = this.props.computedMatch.params;
    this.empId = parseInt(empId);
    const { cookies, history } = props;
    if (cookies.get('emid') != this.empId) {
      history.push('/login');
    }
    this.state = {
      loading: true,
      emp: null,
    };
  }

  onActive = nextIndex => {
    this.setState({
      index: nextIndex,
    });
  };

  componentDidMount() {
    this.findByUserId();
  }

  /** Finding employee by user id */
  findByUserId = () => {
    const { history } = this.props;
    var result = employees.filter(e => {
      return e.id === this.empId;
    });
    if (result.length) {
      this.isFound = true;
      this.setState({
        emp: result[0],
        loading: false,
      });
    } else {
      history.push('/not-found');
    }
  };

  /*** Get company Info to display */
  getCompanyName = id => {
    var result = company.filter(e => {
      return e.id === id;
    });

    if (result.length) {
      return result[0];
    } else {
      return null;
    }
  };

  render() {
    const { loading, emp } = this.state;
    if (loading) {
      return <Box>Loading..</Box>;
    }
    return (
      <Box>
        <TopBar />
        <Box margin="medium" align="center">
          <Box direction="row" align="center">
            <Heading level="4">About Me</Heading>
            <CaretNext />
          </Box>

          <Box direction="row-responsive" wrap>
            <Heading level="2">{emp.name}</Heading>
          </Box>

          <Box margin="small" gap="small" pad="medium" background="white" fill align="center">
            <Box direction="row" justify="between" gap="medium">
              <Box direction="row" gap="medium">
                <Box direction="row" gap="small">
                  <Phone />
                  <Text>{emp.mobile}</Text>
                </Box>
              </Box>
            </Box>
          </Box>
        </Box>
        <Box margin="medium" direction="row-responsive">
          {emp.experiance.map((exp, key) => {
            const companyInfo = this.getCompanyName(exp.company);
            return (
              <Box background="white" margin="small" pad="small">
                <Text>{companyInfo.name}</Text>
                <Text>{exp.position}</Text>
                <Text>{`${exp.duration.from} - ${exp.duration.to}`}</Text>
                <Text>{exp.isCurrent ? 'Currently Working Here' : 'Previous Employement'}</Text>
              </Box>
            );
          })}
        </Box>
      </Box>
    );
  }
}

export default withCookies(withRouter(EmployeeDashboard));
