import React, { Component } from 'react';
import { Box, Heading, Text, Anchor, Tabs, Tab } from 'grommet';

import { withRouter } from 'react-router-dom';
import { CaretNext, Apps, Link, Map, Group, TreeOption, Phone, Alert } from 'grommet-icons';
import { company, employees } from '../constants/data';
import TopBar from '../components/TopBar';

class CompanyDashboard extends Component {
  companyId = null;
  isFound = false;
  constructor(props) {
    super(props);
    const { companyId } = this.props.match.params;
    this.companyId = parseInt(companyId);
    this.state = {
      loading: true,
      company: null,
      empList: null,
    };
  }

  onActive = nextIndex => {
    this.setState({
      index: nextIndex,
    });
  };

  componentDidMount() {
    this.findByCompanyId();
  }

  /***
   * Find company info
   */
  findByCompanyId = () => {
    const { history } = this.props;
    var result = company.filter(com => {
      return com.id === this.companyId;
    });
    if (result.length) {
      this.isFound = true;
      this.setState({
        company: result[0],
        loading: false,
      });
      this.findAllEmployees();
    } else {
      history.push('/not-found');
    }
  };

  findAllEmployees = () => {
    let emp = employees.map(emp => {
      let exp = emp.experiance.filter(com => {
        return com.company === this.companyId;
      });
      if (exp.length) {
        exp[0]['name'] = emp.name;
        return exp[0];
      }
    });
    this.setState({
      empList: emp,
    });
  };

  render() {
    const { loading, company, empList } = this.state;
    console.log(empList);
    if (loading) {
      return <Box>Loading..</Box>;
    }
    return (
      <Box>
        <TopBar />
        <Box margin="medium" align="center">
          <Box direction="row" align="center">
            <Heading level="4">About Us</Heading>
            <CaretNext />
          </Box>

          <Box direction="row-responsive" wrap>
            <Heading level="2">{company.name}</Heading>
          </Box>

          <Box margin="small" gap="small" pad="medium" background="white" fill align="center">
            <Box direction="row" justify="between" gap="medium">
              <Box direction="row" gap="medium">
                <Box direction="row" gap="small">
                  <Group />
                  <Text>{company.employees}</Text>
                </Box>
                <Box direction="row" gap="small">
                  <TreeOption />
                  <Text>Branches</Text>
                  <Text>{company.branches.length}</Text>
                </Box>
              </Box>
            </Box>

            <Box direction="row" gap="small">
              <Apps />
              <Text>Category</Text>
              <Text>{company.category}</Text>
            </Box>
            <Box direction="row" align="center">
              <Map />
              <Text>{company.location}</Text>
            </Box>
            <Box direction="row" margin={{ vertical: 'small' }}>
              <Anchor
                color="#444444"
                icon={<Link size="20px" />}
                label={company.website}
                href={`https:${company.website}`}
                target="_blank"
              />
            </Box>
          </Box>
        </Box>
        <Box margin="medium">
          <Tabs fill>
            <Tab title="Employee List">
              <Box pad="medium">
                {empList &&
                  empList.map((emp, key) => {
                    if (!emp) {
                      return;
                    }
                    const isNow = emp.isCurrent;
                    const background = isNow ? '#ffdfba' : '#999999';
                    const color = isNow ? 'black' : 'white';
                    return (
                      <Box key={`emp-${key}`} background={background} margin="small" pad="medium">
                        <Text color={color}>{emp.name}</Text>
                        <Text color={color}>{emp.position}</Text>
                        <Box direction="row">
                          <Text color={color}>{emp.duration.from}</Text>-
                          <Text color={color}>{emp.duration.to}</Text>
                        </Box>
                        {!isNow ? (
                          <Box direction="row" align="center" gap="small">
                            <Alert color={color} />
                            <Text color={color}>Previously worked here</Text>
                          </Box>
                        ) : null}
                      </Box>
                    );
                  })}
              </Box>
            </Tab>
            <Tab title="Branche List">
              <Box pad="medium" direction="row-responsive" wrap>
                {company.branches.map((branch, key) => {
                  return (
                    <Box background="background" pad="medium" margin="small" key={`branch-${key}`}>
                      <Box direction="row" align="center">
                        <Map />
                        <Text>{branch.address}</Text>
                      </Box>
                      <Box direction="row" align="center">
                        <Phone />
                        <Text>{branch.phone}</Text>
                      </Box>
                    </Box>
                  );
                })}
              </Box>
            </Tab>
          </Tabs>
        </Box>
      </Box>
    );
  }
}

export default withRouter(CompanyDashboard);
