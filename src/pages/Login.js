import React, { Component } from 'react';

import { Anchor, Box, Button, Heading, Paragraph, TextInput, Grommet, Tab, Tabs } from 'grommet';
import { withCookies, Cookies } from 'react-cookie';
import { Next, LinkPrevious } from 'grommet-icons';
import { COLORS } from '../styles';

import { withRouter } from 'react-router-dom';
import { employees } from '../constants/data';

class Login extends Component {
  emp = employees;
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      activeIndex: 0,
      formFields: [],
    };
  }

  setActiveIndex = index => {
    this.setState({
      activeIndex: index,
    });
  };

  onTextChange = (e, type) => {
    this.setState({
      [type]: e,
    });
  };

  login = () => {
    const { mobile, password } = this.state;
    if (mobile && password) {
      this.findUser(mobile, password);
    }
  };

  findUser = (mobile, password) => {
    const { cookies, history } = this.props;
    var result = this.emp.filter(em => {
      return em.mobile == mobile && em.password == password;
    });
    console.log(result);
    if (result.length) {
      cookies.set('emid', result[0].id, { path: '/' });
      history.push(`/employee/${result[0].id}`);
    }
    // alert('Credentials Invalid');
  };

  render() {
    const { activeIndex } = this.state;
    return (
      <Box background="white" fill justify="center" align="center">
        <Box
          margin="small"
          direction="row"
          justify="center"
          gap="small"
          style={{
            position: 'absolute',
            top: 10,
            left: 10,
          }}
        >
          <LinkPrevious color={COLORS.secondary} />
          <Anchor label="Back to Home" href="/" color={COLORS.secondary} />
        </Box>
        <Heading>Sign In </Heading>

        <Paragraph textAlign="center">Hello there! </Paragraph>

        <Grommet>
          <Tabs activeIndex={activeIndex} onActive={this.setActiveIndex}>
            {['Login', 'Sign Up'].map((item, key) => {
              return (
                <Tab title={item} key={`tab-bar-${key}`}>
                  {key ? (
                    <Box
                      pad="small"
                      margin="medium"
                      gap="medium"
                      width="500px"
                      height="350px"
                      background={COLORS.white}
                    >
                      <TextInput
                        placeholder="Mobile Number"
                        onChange={e => {
                          this.onTextChange(e.target.value, 'mobile');
                        }}
                        size="small"
                        plain
                        style={{
                          backgroundColor: COLORS.background,
                        }}
                      />
                      <TextInput
                        placeholder="Password"
                        onChange={e => {
                          this.onTextChange(e.target.value, 'password1');
                        }}
                        size="small"
                        type="password"
                        plain
                        style={{
                          backgroundColor: COLORS.background,
                        }}
                      />
                      <TextInput
                        placeholder="Password"
                        onChange={e => {
                          this.onTextChange(e.target.value, 'password2');
                        }}
                        size="small"
                        type="password"
                        plain
                        style={{
                          backgroundColor: COLORS.background,
                        }}
                      />

                      <Button
                        primary
                        color="black"
                        icon={<Next />}
                        reverse
                        label="Sign Up"
                        onClick={this.createAccount}
                        size="small"
                      />
                    </Box>
                  ) : (
                    <Box pad="small" margin="medium" gap="medium" width="500px" height="350px">
                      <TextInput
                        placeholder="Mobile Number"
                        onChange={e => {
                          this.onTextChange(e.target.value, 'mobile');
                        }}
                        size="small"
                        plain
                        style={{
                          backgroundColor: COLORS.background,
                        }}
                      />
                      <TextInput
                        placeholder="Password"
                        onChange={e => {
                          this.onTextChange(e.target.value, 'password');
                        }}
                        size="small"
                        type="password"
                        plain
                        style={{
                          backgroundColor: COLORS.background,
                        }}
                      />
                      <Button
                        primary
                        icon={<Next />}
                        color="black"
                        reverse
                        label="Login"
                        onClick={this.login}
                        size="small"
                      />
                    </Box>
                  )}
                </Tab>
              );
            })}
          </Tabs>
        </Grommet>
      </Box>
    );
  }
}

export default withCookies(withRouter(Login));
